public class Human{
    private String name;
    private double money;
    private int hunger;
    private int boredom;

    public Human(String humanName){
        this.name = humanName;
        this.money=0;
        this.hunger=0;
        this.boredom=0;
    }

    public boolean doWork(){
        if(this.hunger > 50){
            System.out.println(this.name+" is too hungry to work");
            return false;
        }else if(this.boredom > 50){
            System.out.println(this.name+" is too bored to work");
            return false;
        }
        money += 20;
        hunger += 5;
        boredom += 10;
        System.out.println(this.name+" did the work YAYYYY!");
        return true;
    }
}