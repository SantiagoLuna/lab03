public class Dog {
    private String name; 
    private int energy;
    private int hunger;
    private int boredom;

    public Dog (String name, int energy, int hunger, int boredom){
        this.name = name;
        this.energy = energy;
        this.hunger = hunger;
        this.boredom = boredom;
    }

    public boolean takeNap(){
        if (this.hunger > 50 && this.boredom > 50){
            this.energy = this.energy + 20;
            this.hunger = this.hunger + 5;
            this.boredom = this.boredom + 10;
            return true;
        }
        else {
            if (this.hunger < 50) {
                System.out.println("too hungry to nap");
            }
            else if (this.boredom < 50) {
                System.out.println("Too bored to nap");
            }
            return false;
        }
    }
}